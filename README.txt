CLICK2CALL.module
--------------------------------------------------------------------------------
This module provides:
 - a new CCK field that activates a call script whenever a user presses the button provided
 - configurable number of blocks with a call script whenever a user presses the button provided

--------------------------
Dependencies
This module depends on following modules:
CCK
Voip

--------------------------
Installation

1. Extract Click2Call to your sites/all/modules directory. 
2. Enable the Click2Call module in admin/build/modules.

To use Click To Call CCK:
1. Choose any content type from admin/content/types and go to "manage fields".
2. Add a new field, select Click To Call Field as its field type and Click To Call Field as the widget.
3. Set per field options (for more details see Options)
4. Save.
5. Create new content and you will see the Click To Call field. Choose call script and short script description. 
6. Save the node.
7. Now in newly created node you will see link with script description. When clicked on link it will expand to a small form where user can choose his number 
and click the button to place a call.
8. To allow other users to use Click to Call Button in node pages, go to admin/user/permissions and enable "use click to call field" permission for desired 
roles 

To use Click To Call Block:
1. Go to admin/build/block and you will see Click to Call blocks.
2. Click on configure and choose call script and short script description. 
2. Assign block to a region and you will see link with script description. When clicked on link it will expand to a small form where user can choose his number 
and click the button to place a call.

Note: By default this module provides 2 Click to Call blocks. To create more go to admin/settings/Click2Call and enter number of blocks you wish to create.

--------------------------
Options

You can select how the users will choose number to be called.
1. Default number (from user profile)* - will get number from user profile or display appropriate message to fill the number if empty.
2. Number returned by Voipnumber field* - select list with all numbers from Voipnumber CCK fields of that user. Will not be shown if empty.
3. Allow user to type in the number - textfield to allow user to type any number.

* this options are available only if Voipnumber is enabled.
---
The Click2Call module has been originally developed by Tamer Zoubi under the sponsorship of the MIT Center for Future Civic Media (http://civic.mit.edu).




